var express = require('express')
var mongojs = require('mongojs')
var app = express()
var db = mongojs('mongodb://localhost/esp8266')
var raw_data = db.collection('data')
app.get('/', function (req, res) {
  res.send('UP');
})

app.get('/push/:value', function (req, res) {
    raw_data.save({
        value: req.params.value,
        createdAt: new Date()
    }, function (err, doc) {
        res.send('OK');
    })
    console.log(req.params.value);
})

app.use('/display',express.static('display'));

app.get('/data', (req,res)=>{
    raw_data.find().sort({_id:-1}).limit(500, function(err,doc){
        if(doc){
            res.json(doc)
        }else{
            res.json([]);
        }
    });
});
app.get('/last', (req,res)=>{
    raw_data.find().sort({_id:-1}).limit(1, function(err,doc){
        if(doc){
            res.json(doc[0])
        }else{
            res.json([]);
        }
    });
});
db.on('connect', function () {
    console.log('database connected')
});

db.on('error', function (err) {
    console.log('database error', err)
});

app.listen(3000)